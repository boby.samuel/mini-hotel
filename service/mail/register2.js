var nodemailer = require('nodemailer');
const fs = require('fs')

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: '',
    pass: ''
  }
});

const template = ({ name, link }) => 
`
<b>Welcome ${name} to our App !!!!.</b>

<p> click this link to verify your account ${link}</p>
`

exports.sendMail = ({ recipient, verificationLink }) => {
  try {
    console.log(`sending email to ${recipient}`)
    const nameFromEmail = recipient.split('@')[0]
    const mailOptions = {
      from: 'chefbox',
      to: recipient,
      subject: 'Your registration is complete',
      html: template({ name: nameFromEmail, link: verificationLink }),
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }
  catch(err) {
    throw err
  }
}