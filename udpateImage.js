// exports.upadateUserValidator = async (req, res, next) => {
//   try {
//     const errors = [];

const { Router } = require("express")

//     //Check input of Name
//     if (validator.isEmpty(req.body.name, { ignore_whitespace: true })) {
//       errors.push("Please input the Name!");
//     }

//     //Check input of Email
//     if (!validator.isEmail(req.body.email)) {
//       errors.push("Email is not valid");
//     }

//     // if update password
//     if (req.body.password) {
//       if (
//         !validator.isStrongPassword(req.body.password, [
//           {
//             minLength: 10,
//             minLowercase: 1,
//             minUppercase: 1,
//             minNumbers: 1,
//             minSymbols: 1,
//             maxLength: 20,
//           },
//         ])
//       ) {
//         errors.push(
//           "password must include lowercase: min 1, uppercase: min 1, numbers: min 1, symbol: min 1, and length: min 10 characters & max 20 characters."
//         );
//       }

//       if (req.body.password !== req.body.confirmPassword) {
//         errors.push("password and confirm password didn't match!");
//       }
//     }

//     // For optional image
//     const findUser = await user.findOne({
//       where: { email: req.body.email },
//     });

//     if (!(req.files && req.files.image)) {
//       req.body.image = findUser.image;
//     } else if (req.files.image) {
//       // req.files.photoUser is come from key (file) in postman
//       const file = req.files.image;

//       // Make sure image is photo
//       if (!file.mimetype.startsWith("image")) {
//         errors.push("File must be an image");
//       }

//       // Check file size (max 1MB)
//       if (file.size > 1000000) {
//         errors.push("Image must be less than 1MB");
//       }

//       // Create custom filename
//       let fileName = crypto.randomBytes(16).toString("hex");

//       // Rename the file
//       file.name = `${fileName}${path.parse(file.name).ext}`;

//       // Make file.mv to promise
//       const move = promisify(file.mv);

//       // Upload image to /public/images
//       await move(`./public/images/users/${file.name}`);

//       const image = await cloudinary.uploader
//         .upload(`./public/images/users/${file.name}`)
//         .then((result) => {
//           return result.secure_url;
//         });

//       // assign req.body.image with file.name
//       req.body.image = image;
//     }

//     //Check input of bank name
//     if (validator.isEmpty(req.body.bankName, { ignore_whitespace: true })) {
//       errors.push("Please input bankName!");
//     }

//     //Check input of bank account
//     if (validator.isEmpty(req.body.bankAccount, { ignore_whitespace: true })) {
//       errors.push("Please input bank account number!");
//     }

//     //Check input of bank account
//     if (!validator.isInt(req.body.bankAccount)) {
//       errors.push("Bank account number must be a number!");
//     }

//     if (errors.length > 0) {
//       return res.status(400).json({ errors: errors });
//     }

//     next();
//   } catch (error) {
//     next(error);
//   }













const updateUser = (req, res, next) => {
  const { id: user_id } = req.payload.user
  // {
    // name: 'asdasdasd',
    // bankName: 'asdasdasd',
    // bankAccount: 'adasdasd',
    // password: 'uiyhbaeor8v76g' // optional
    // image: www.cloudinary.com/asdasdasdasd.jpg // optional
    // email: 'asdasdasd'
  // }
  
  const { image, password, ...finalData } = req.body
  if (image) finalData['image'] = image
  if (password) finalData['password'] = password

  const updatedUserData = await User.update(finalData, { 
    where: { id: user_id },
    returning: true
  })

  res.status(200).json({
    success: true,
    data: updatedUserData
  })
}


Router.post('/imageupload') 